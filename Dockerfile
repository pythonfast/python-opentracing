FROM python:alpine

WORKDIR app

RUN pip3 install jaeger-client opentracing

COPY jaegerPython $WORKDIR