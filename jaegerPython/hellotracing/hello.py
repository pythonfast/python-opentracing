# a simple hello world program
# to demonstrate instrumenting a
# simple Python program using OpenTracing API
# and JaegerTracing system

import sys
import time
import logging
from jaeger_client import Config

def init_jaeger_tracer(service):
    logging.getLogger('').handlers=[]
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    config=Config(
        config={
            'sampler':{
                'type':'const',
                'param':1
            },
            'logging':True
        },
        service_name=service
    )

    return config.initialize_tracer()

tracer = init_jaeger_tracer('hello-world')

def say_hello(hello_to):
    with tracer.start_span('say-hello') as span:
        hello_str = "Hello, %s!" % hello_to
        print(hello_str)

assert len(sys.argv) == 2

hello_to = sys.argv[1]
say_hello(hello_to)

time.sleep(20)
tracer.close()